# Twitter Export Viewer

An viewer for Twitter GDPR exports

## Usage

You can make use of this tool either as a single file (easiest to use) or separate files (easiest to contribute to)

### Single File

1. Unzip a twitter GDPR extract
2. Download the `viewer-singlepage.html` from [here](./raw/master/viewer-singlepage.html), into the same directory
3. Open the `viewer-singlepage.html`
4. Check console for instructions for twitter handles and images

### Separate Files

1. Clone repository
2. Unzip a twitter GDPR extract into repository -**or**- publish project files via ssh/s3 using `make deploy dest=<target>`
3. Open `viewer.html`
4. Check console for instructions for twitter handles and images

## Installing, Linting, Building Single File, Deployment, Contributing

see `make help`

Including instructions for deploying locally, ssh, s3, etc

When contributing always `build` and `commit` `viewer-singlepage.html` to make the project easy to use.

## TODO

* Add ability to read files locally
* Add Example deployment

## Authors

* **[Mataza89](https://reddit.com/u/Mataza89)** - *Single page version*
* **[RiotingPacifist](https://gitlab.com/RiotingPacifist)** - *Build into project*

## Copyright

Twitter Export Viewer is licensed under the GNU General Public License, v3 or later.
A copy of this license is included in the file [LICENSE](LICENSE).

Copyright 2019, Mataza89, RiotingPacifist and contributors.
