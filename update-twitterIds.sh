#!/usr/bin/env sh
# This script is simply to show what is being echod to the console, it is not compiled into it
if [ ! -f twitterIDs.js ]; then
    echo "var twitterIDs = {" >> ./twitterIDs.js
else
    sed -i '/^}$/d' twitterIDs.js
fi
for id in ${ids} ; do
    curl -s "https://twitter.com/intent/user?user_id=$id" > /tmp/twitterProf.html
    name="$(grep '<title>' /tmp/twitterProf.html | sed 's/\\ on Twitter<.*//; s/.*>//')"
    bio="$(grep 'class="note">' /tmp/twitterProf.html | sed 's/.*"note">//; s/<.*//')"
    echo \"$id\": {\"name\":\"$name\", \"bio\":\"$bio\"},
done | grep -v '{"name :"", "bio":""}' | tee -a ./twitterIDs.js
echo "}" | tee -a ./twitterIDs.js
