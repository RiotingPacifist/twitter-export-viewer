#!/usr/bin/env sh
# This script is simply to show what is being echod to the console, it is not compiled into it
for dir in direct_message_media direct_message_group_media; do
    pushd $dir
    unzip *.zip
    for file in *; do
        mv $f ${f##*[0-9\\-]-}
    done
    popd
done
