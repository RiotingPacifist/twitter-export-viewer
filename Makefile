FILES=viewer.css viewer.js viewer.html early-vars.js 
CP_CMD="scp"
AWS_CP_CMD="aws s3 cp"
ifneq (,$(findstring s3://,$(dest)))
  cmd ?= $(AWS_CP_CMD)
else
  cmd ?= $(CP_CMD)
endif
.PHONY: help lint clean prepare build deploy deploy-single all all-single'

help:
	@echo  'Targets:'
	@echo  '  install 	- install build dependencies'
	@echo  '  lint 		- lint files'
	@echo  '  clean 	- clean dependencies'
	@echo  '  prepare 	- clean, install, lint'
	@echo  '  build 	- build single page veiwer'
	@echo
	@echo  '  deploy 	- deploy files to dest'
	@echo  '  deploy-single - puslish single page viewer'
	@echo  '  all		- run pre-build, build (if applicable) and deploy'
	@echo  '  all-single'
	@echo
	@echo  'Variables (needed for deploy, deploy-signle, all, all-single)'
	@echo  '  dest=<location to copy files to>'
	@echo  '  cmd=<command used to copy files>'
	@echo  '    cmd defaults to $(CP_CMD)'
	@echo  '    if dest starts with "s3://" cmd will be $(AWS_CP_CMD)'

install:
	npm install .

lint:
	npm run lint

build: lint
	npm run build

clean:
	npm run clean
	rm -r node_modules/ package-lock.json

_check_dest:
	$(if $(value dest),, $(error dest=<location> must be specified e.g make deploy dest=ssh://server/var/html/))

deploy: _check_dest
	$(foreach file,$(FILES), $(cmd) $(file) $(dest);)

deploy-single: _check_dest
	$(cmd) viewer-singlepage.html $(dest)

prepare: clean install lint

all: prepare deploy

all-single: clean install build deploy-single
