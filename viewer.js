// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

if (undefined === twitterIDs) { var twitterIDs = {} } // eslint-disable-line no-use-before-define
var unkownIds = []
var BrokenImageLogged = false

function loadOwnUsername () {
  var account = window.YTD.account.part0[0].account
  twitterIDs[account.accountId] = {
    name: account.accountDisplayName,
    bio: window.YTD.profile.part0[0].profile.description.bio
  }
}

function renderPage () { // eslint-disable-line no-unused-vars
  var initialUnkownIdCount = unkownIds.length
  var container = document.getElementById('sender_container')
  container.innerHTML = ''
  if (document.getElementById('direct_messages').checked) {
    container.innerHTML += window.YTD.direct_message.part0.filter(filterConversation).map(renderDirectMessageConversation).join('')
  }
  if (document.getElementById('group_messages').checked) {
    container.innerHTML += window.YTD.direct_message_group.part0.filter(filterConversation).map(renderGroupMessageConversation).join('')
  }
  if (unkownIds.length > initialUnkownIdCount) {
    var ids = unkownIds.join(' ')
    console.log("The following twitter Ids don't have a know user, if you want to find one, add them to the map in the html file ", unkownIds.join())
    console.log('The following script should generate a valid list (but if the file exist manual fixing is needed)')
    console.log(`if [ -f twitterIDs.js ]; then sed -i '/^}$/d' twitterIDs.js ; else echo "var twitterIDs = {" >> ./twitterIDs.js ; fi ; for id in ${ids} ; do curl -s "https://twitter.com/intent/user?user_id=$id" > /tmp/twitterProf.html && echo \\"$id'": { "name" : "'$(grep '<title>' /tmp/twitterProf.html | sed 's/\\ on Twitter<.*//; s/.*>//')'", "bio":"'$(grep 'class="note">' /tmp/twitterProf.html | sed 's/.*"note">//; s/<.*//')'"},'; done | grep -v '{ "name" : "", "bio":""}' | tee -a ./twitterIDs.js; echo "}" | tee -a ./twitterIDs.js`)
  }
}

function filterConversation (conversation) {
  var messages = conversation.dmConversation.messages
  if (!(document.getElementById('recipient_threads').checked || messages.filter(isMessage).some(accountIsSender))) {
    return false
  }
  if (document.getElementById('search').value) {
    if (messages.filter(isMessage).filter(
      function (message) {
        return !document.getElementById('search_sender').checked || accountIsSender(message)
      }
    ).every(
      function (message) {
        return message.messageCreate.text.toLowerCase().indexOf(document.getElementById('search').value.toLowerCase()) < 0
      }
    )) {
      return false
    }
  }
  return true
}

function isMessage (message) {
  return message.hasOwnProperty('messageCreate')
}

function accountIsSender (message) {
  return message.messageCreate.senderId === window.YTD.account.part0[0].account.accountId
}

function renderDirectMessageConversation (conversation) {
  var openDetails = getDetailsOpenTag()
  var messages = conversation.dmConversation.messages.map(renderMessage.bind(this, 'direct_message')).join('\n ')
  var headers = addDirectMessageHeader(conversation.dmConversation.messages)

  return `${openDetails}\n ${headers}\n ${messages}\n</details>`
}

function addDirectMessageHeader (messages) {
  var message = messages[0]
  var sender = getUserLink(message.messageCreate.senderId)
  var recipient = getUserLink(message.messageCreate.recipientId)
  var startDate = new Date(message.messageCreate.createdAt).toLocaleString()
  var endDate = new Date(messages[messages.length - 1].messageCreate.createdAt).toLocaleString()

  return `<summary> ${messages.length} messages ${sender} to ${recipient} from ${startDate} to ${endDate}</summary>`
}

function renderGroupMessageConversation (conversation) {
  var openDetails = getDetailsOpenTag()
  var messages = conversation.dmConversation.messages
  var headers = addGroupMessageHeader(messages)
  var messagesText = messages.slice(0, messages.length - 1).map(renderMessage.bind(this, 'direct_message_group')).join('\n ')

  return `${openDetails}\n ${headers}\n ${messagesText}\n</details>`
}

function addGroupMessageHeader (messages) {
  var headers = messages[messages.length - 1].joinConversation
  var initiatingUser = getUserLink(headers.initiatingUserId)
  var startDate = new Date(headers.createdAt).toLocaleString()
  var endDate = startDate

  if (isMessage(messages[0])) {
    endDate = new Date(messages[0].messageCreate.createdAt).toLocaleString()
  } else if (messages[0].hasOwnProperty('participantsLeave')) {
    endDate = new Date(messages[0].participantsLeave.createdAt).toLocaleString()
  } else if (messages[0].hasOwnProperty('participantsJoin')) {
    endDate = new Date(messages[0].participantsJoin.createdAt).toLocaleString()
  }

  var particpants = addUserList(headers.participantsSnapshot)
  return `<summary> ${messages.length - 1} messages ${particpants} started by ${initiatingUser} from ${startDate} to ${endDate}</summary>`
}

function getDetailsOpenTag () {
  if (document.getElementById('expand').checked) {
    return '<details class="conversation" open=true >'
  } else {
    return '<details class="conversation" >'
  }
}

function renderMessage (messageType, message) {
  /* eslint-disable no-redeclare */

  if (isMessage(message)) {
    var sender = getUserLink(message.messageCreate.senderId)
    var date = new Date(message.messageCreate.createdAt).toLocaleString()
    var sent = accountIsSender(message)
    var recipient = ''

    if (message.messageCreate.hasOwnProperty('recipientId')) {
      recipient = ' to ' + getUserLink(message.messageCreate.recipientId)
    }

    var string = `<div class="message_container" sent="${sent}"><div class="sender"> ${sender} @ ${date}${recipient}</div>`
    message.messageCreate.mediaUrls.forEach(function (mediaUrl) {
      var urlParts = mediaUrl.split('/')
      var src = messageType + '_media/' + urlParts[urlParts.length - 1]
      string = string + `<a href="${src}"><img src="${src}" onError="logBrokenImage()" alt="check console logs"></a>`
    })
    string = string + `<div class="message">${message.messageCreate.text}</div></div>`
    return string
  } else if (message.hasOwnProperty('participantsLeave')) {
    var date = new Date(message.participantsLeave.createdAt).toLocaleString()
    var userList = addUserList(message.participantsLeave.userIds)
    return `<div class="message_container" class="action">${userList} left @ ${date}</div>`
  } else if (message.hasOwnProperty('participantsJoin')) {
    var date = new Date(message.participantsJoin.createdAt).toLocaleString()
    var userList = addUserList(message.participantsJoin.userIds)
    var initiatingUser = getUserLink(message.participantsJoin.initiatingUserId)
    return `<div class="message_container" class="action">${userList} were added @ ${date} by ${initiatingUser} </div>`
  }
  /* eslint-enable */

  console.log('unhandled message', messageType, message)
  return ''
}

function addUserList (usersIds) {
  var users = usersIds.map(getUserLink)
  return users.join(', ')
}

function getUserLink (id) {
  if (twitterIDs[id] === undefined) {
    if (unkownIds.indexOf(id) < 0) {
      unkownIds.push(id)
    }
    return `<a href="https://twitter.com/intent/user?user_id=${id}">${id}</a>`
  } else {
    var name = twitterIDs[id].name
    var bio = twitterIDs[id].bio
    return `<a href="https://twitter.com/intent/user?user_id=${id}" alt=${bio}>${name}</a>`
  }
}

function logBrokenImage () { // eslint-disable-line no-unused-vars
  if (!BrokenImageLogged) {
    BrokenImageLogged = true
    console.log('An Image has failed to load, to unpack images please run')
    console.log('for dir in direct_message_media direct_message_group_media; do pushd $dir ; unzip *.zip ; for f in *; do mv $f ${f##*[0-9\\-]-}; done ; popd ; done')
  }
}

loadOwnUsername()

// @license-end
